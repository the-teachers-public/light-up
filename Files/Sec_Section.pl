/*
Fact: lighted(_,_) so i can dynamicly store.
*/
lighted(_,_):-!.

/*
for printing the Grid.
*/

printing(X,Y):- wall_num(X,Y,Z),write("WB"),write(Z),!.
printing(X,Y):- wall(X,Y),write('UWB'),!.
printing(X,Y):- light(X,Y),write('Lit'),!.
printing(X,Y):- check(X,Y),write(' + '),!.
printing(_,_):-write(' * '),!.



/*
print_grid: for printing the Grid
*/
loop_row(Z,_):- fixed_size(N1),Z=N1,!.
loop_row(Z,X):-  loop_col(Z,X),nl,nl,Z1 is Z+1,loop_row(Z1,X).


loop_col(_,X):-fixed_size(N1),X=N1,!.
loop_col(Z,X):- printing(Z,X),write("\t"),X1 is X+1,loop_col(Z,X1).

print_grid:-loop_row(1,1),!.

/*
add_light: to put a light in the Grid,to make boxes lighted based on light postion.
*/

add_lightd(X,_):-fixed_size(N1),X=N1,!.
add_lightd(X,Y):-wall(X,Y),!.
add_lightd(X,Y):-
asserta(lighted(X,Y)),
X1 is X+1,add_lightd(X1,Y).

add_lightu(0,_):-!.
add_lightu(X,Y):-wall(X,Y),!.
add_lightu(X,Y):-
asserta(lighted(X,Y)),
X2 is X-1,add_lightu(X2,Y).

add_lightr(_,Y):-fixed_size(N1),Y=N1,!.
add_lightr(X,Y):-wall(X,Y),!.
add_lightr(X,Y):-
asserta(lighted(X,Y)),
Y1 is Y+1,add_lightr(X,Y1).

add_lightl(_,0):-!.
add_lightl(X,Y):-wall(X,Y),!.
add_lightl(X,Y):-
asserta(lighted(X,Y)),
Y2 is Y-1,add_lightl(X,Y2).

add_light(X,Y):- add_lightd(X,Y),add_lightl(X,Y),
add_lightr(X,Y),add_lightu(X,Y).



/*
function: return true if the Box is lighted.
*/
check(X,Y):- lighted(X,Y),!.


/*
function: Grid initialization
*/

loop_init2(_,Y):-fixed_size(N1),Y=N1,!.
loop_init2(X,Y):-not(light(X,Y)),Y1 is Y+1,loop_init2(X,Y1).
loop_init2(X,Y):-light(X,Y),add_light(X,Y),Y1 is Y+1,loop_init2(X,Y1).

loop_init(X):-fixed_size(N1),X=N1,!.
loop_init(X):-loop_init2(X,1),X1 is X+1,loop_init(X1).

init:- loop_init(1),!.

/*
light_count_correct: to check the wall_numbered if the lights around the wall > number
*/
count_down(1,Y,Z):-count_up(1,Y,Z),!.
count_down(X,Y,Z):- X1 is X-1,\+light(X1,Y),count_up(X,Y,Z),!.
count_down(X,Y,Z):- X1 is X-1,light(X1,Y),Z1 is Z+1,count_up(X,Y,Z1).

count_up(X,Y,Z):- size(_,N1),X=N1,count_left(X,Y,Z),!.
count_up(X,Y,Z):- X2 is X+1,\+light(X2,Y),count_left(X,Y,Z),!.
count_up(X,Y,Z):- X2 is X+1,light(X2,Y),Z1 is Z+1,count_left(X,Y,Z1).

count_left(X,1,Z):-count_right(X,1,Z),!.
count_left(X,Y,Z):- Y1 is Y-1,\+light(X,Y1),count_right(X,Y,Z),!.
count_left(X,Y,Z):- Y1 is Y-1,light(X,Y1),Z1 is Z+1,count_right(X,Y,Z1).

count_right(X,Y,Z):-size(_,N1),Y=N1,test_numbered_wall(X,Y,Z),!.
count_right(X,Y,Z):- Y2 is Y+1,\+light(X,Y2),test_numbered_wall(X,Y,Z),!.
count_right(X,Y,Z):- Y2 is Y+1,light(X,Y2),Z1 is Z+1,test_numbered_wall(X,Y,Z1).

test_numbered_wall(X,Y,Z):- wall_num(X,Y,L),L>=Z.
count_num(X,Y):- count_down(X,Y,0).


loop_light_count2(_,Y):-fixed_size(N1),Y=N1,!.
loop_light_count2(X,Y):-not(wall_num(X,Y,_)),Y1 is Y+1,loop_light_count2(X,Y1).
loop_light_count2(X,Y):-wall_num(X,Y,_),count_num(X,Y),Y1 is Y+1,loop_light_count2(X,Y1).

loop_light_count(X):-fixed_size(N1),X=N1,!.
loop_light_count(X):-loop_light_count2(X,1),X1 is X+1,loop_light_count(X1).


light_count_correct:-loop_light_count(1),!.

/*
all_cells_lighted: to check if all the boxes is lighted.
*/
loop_lighted2(_,Y):-fixed_size(N1),Y=N1,!.
loop_lighted2(X,Y):-wall(X,Y),Y1 is Y+1,loop_lighted2(X,Y1).
loop_lighted2(X,Y):-light(X,Y),Y1 is Y+1,loop_lighted2(X,Y1).
loop_lighted2(X,Y):-check(X,Y),Y1 is Y+1,loop_lighted2(X,Y1).

loop_lighted(X):-fixed_size(N1),X=N1,!.
loop_lighted(X):-loop_lighted2(X,1),X1 is X+1,loop_lighted(X1).


all_cells_lighted:-loop_lighted(1),!.

/*
no_double_light: to check if there is a double light
*/
loop2(_,Y):-fixed_size(N1),Y=N1,!.
loop2(X,Y):-not(light(X,Y)),Y1 is Y+1,loop2(X,Y1).
loop2(X,Y):-light(X,Y),can_add_light(X,Y),Y1 is Y+1,loop2(X,Y1).

loop(X):-fixed_size(N1),X=N1,!.
loop(X):-loop2(X,1),X1 is X+1,loop(X1).

no_double_light:-loop(1),!.

check_up(X,Y):-X=0;wall(X,Y).
check_up(X,Y):-X>=0,not(wall(X,Y)),not(light(X,Y)),X1 is X-1,check_up(X1,Y).

check_down(X,Y):-fixed_size(N1),X=N1,!;wall(X,Y),!.
check_down(X,Y):-not(wall(X,Y)),not(light(X,Y)),X1 is X+1,check_down(X1,Y).

check_right(X,Y):-fixed_size(N1),Y=N1,!;wall(X,Y),!.
check_right(X,Y):-not(wall(X,Y)),not(light(X,Y)),Y1 is Y+1,check_right(X,Y1).

check_left(X,Y):-Y=0;wall(X,Y).
check_left(X,Y):-Y>0,not(wall(X,Y)),not(light(X,Y)),Y1 is Y-1,check_left(X,Y1).

can_add_light(X,Y):-not(wall(X,Y)),X1 is X-1,check_up(X1,Y),X2 is X+1,check_down(X2,Y)
,Y1 is Y-1,check_left(X,Y1),Y2 is Y+1,check_right(X,Y2).




/*
solved: check is this salotion is true or not.
*/

solved:- init,all_cells_lighted,
no_double_light ,
light_count_correct,print_grid .




/*
solve_it: dynamic solution.
*/
loop_solve2(_,Y):-fixed_size(N1),Y=N1,!.
loop_solve2(X,Y):-wall(X,Y),Y1 is Y+1,loop_solve2(X,Y1).
loop_solve2(X,Y):-check(X,Y),Y1 is Y+1,loop_solve2(X,Y1).
loop_solve2(X,Y):-not(check(X,Y)),can_add_light(X,Y),asserta(light(X,Y)),light_count_correct,add_light(X,Y),Y1 is Y+1,print_grid,write("\n\n"),loop_solve2(X,Y1).
loop_solve2(X,Y):- retractall(light(X,Y)),Y1 is Y+1,loop_solve2(X,Y1).

loop_solve(X):-fixed_size(N1),X=N1,!.
loop_solve(X):-loop_solve2(X,1),X1 is X+1,loop_solve(X1).


solve_it:-loop_solve(1),solved,!.