:- dynamic (lighted/2).
:- dynamic (light/2).


/*
clear: is for clearing the ram from dynamic facts for only lighted().
clear_all: is for clearing the ram from dynamic facts for both lighted()&light().
*/
clear:-retractall(lighted(_,_)).
clear_all:-retractall(lighted(_,_)),retractall(light(_,_)).

/*
Facts:
*/