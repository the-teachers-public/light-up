# **_Light-UP_**

## Description
This project is from a subject at the University of Damascus called Principles of Intelligence, which is an introduction to artificial intelligence. <br >
All the rules of the game are written in Prolog. <br >
the entire project consists of two parts, a section for making the board with the solution, and the second is checking the solution if it is correct or not.<br >
Also, inside the game, there is a feature to create a dynamic solution.<br >

___
## Rules:
Light Up is played on a square grid of white and black cells. The player places light bulbs in white cells such that no two bulbs shine on each other until the entire grid is lit up. A bulb sends rays of light horizontally and vertically, illuminating its entire row and column unless its light is blocked by a black cell. A black cell may have a number on it from 0 to 4, indicating how many bulbs can be placed adjacent to its four sides; for example, a cell with a 4 can have four bulbs around it, one on each side, and a cell with a 0 cannot have a bulb next to any of its sides. An unnumbered black cell may have any number of light bulbs adjacent to it or none.
___
## Badges
- ![C++](https://img.shields.io/badge/c++-%2300599C.svg?style=for-the-badge&logo=c%2B%2B&logoColor=white)
- ![SWI-Prolog](https://img.shields.io/badge/swi--prolog-v8.4.3-red)
___

## Visuals
the application has two parts:
- Grid interface for building the grid Note: there is a default grid so you can first test it.
- Prolog interface to test your solution or to let the prolog app make a solution.

## now for the Visuals:
* ### Main Menu:
>> ![Main Menu:](https://gitlab.com/the-teachers-public/light-up/-/raw/main/Visuals/MainMenu.png)<br >
1- To view your grid or the default grid <br >
2- For changing the settings for your grid.<br >
3- To clear the interface for making everything clear and organized.<br >
4- To move to SWI-prolog interface.<br >

* ### view the grid:
>> ![print grid](https://gitlab.com/the-teachers-public/light-up/-/raw/main/Visuals/viewGrid.png)<br >
>> " * ": means this cell is not lighted.<br >
>> " + ": means this cell is lighted.<br >
>> "Lit": this cell contains light.<br >
>> "UWB": this cell contains Unweighted Block.<br >
>> "WBX": this cell contains weighted Block with weight of X ==>[0, ... , 4].<br >

* ### Settings:
>> ![Settings:](https://gitlab.com/the-teachers-public/light-up/-/raw/main/Visuals/settings.png)<br >
1- To ReSize your grid.<br >Note: if you resize the grid it will return to default (all blanks)<br >
2- To add a light.<br >
3- To add a block: Weighted / Unweighted Block.<br >
4- To make a blank if you made a mistake or for editing purposes.<br >
5- Back to Main Menu<br >

* ### finish initializing the grid:
>> ![first part](https://gitlab.com/the-teachers-public/light-up/-/raw/main/Visuals/DoneGridInit.png)<br >

* ### SWI-prolog interface:
>> ![sec part](https://gitlab.com/the-teachers-public/light-up/-/raw/main/Visuals/clear&print_grid.png)<br >
prolog commands will be explained in Usage.
___
## Installation
I made it as easy as possible you only need to download the project and open (.Light-UP) file<br > 
no installation is required 😊 
___
## Usage
#### There is no need for an explanation for the first part:{view, edit, ReSize}.<br > 
#### But in the second part there are some commands you need to know:<br > 
#### 1.clear and clear_all:<br > 
-Clear: remove all Lighted facts.<br > 
Note Lighted: means the element in position (x,y) is lighted by a light.<br > 
-Clear_all: remove all light and lighted facts.<br > 
#### 2.print_grid:<br > 
To print the grid.<br > 
#### 3.init:<br > 
To initialize the grid before testing the solution. <br > 
#### 4.solved:<br > 
 This command contains 3 commands in it:<br > 
- no_double_light: to check if there are two lights without a wall between them.<br > 
- Light_count_correct:  to check on the weighted wall if the number of lights on his edges is less or equal to the weight<br > 
- All_cells_lighted: to check if all cells are lighted.<br > 

#### 5._solve_it_:<br >
to generate a dynamic solution.<br > 
### Note: you must end the command with " . ".
___

## Support

**_For Support you can Contact me at:_** <br >
**_Email:_** Majd_ma@outlook.com.<br >
**_Linked_in:_** [Majd MA](https://www.linkedin.com/in/majd-ma-10067a101).



