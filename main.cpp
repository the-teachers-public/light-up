#include <iostream>
#include <fstream>
#include<vector>
#include<stdlib.h>
#include <Windows.h>
#include <cstdlib>

using namespace std;

ofstream MyFile("Light-UP.pl");
void PrintGride(vector<vector<string>> Grid){
    std::cout << std::endl;
    for (int i = 0; i < Grid.size(); ++i)
    {
        for (int j = 0; j < Grid[i].size(); ++j)
        {
            std::cout << Grid[i][j]<<' ';

        }
        std::cout << std::endl; std::cout << std::endl;
    }
}

void initializeGrid(vector<vector<string>> Grid){

  MyFile<< "size("+to_string(Grid.size())+","+to_string(Grid[0].size())+").\n""fixed_size(N1):-size(_,N), N1 is N+1.""\n\n";

   for (int i = 0; i < Grid.size(); ++i)
        for (int j = 0; j < Grid[i].size(); ++j)
            if(Grid[i][j]!="Lit"&&Grid[i][j]!=" * ")
                 MyFile<<"wall("+to_string(i+1)+","+to_string(j+1)+").\n";
 MyFile<<"\n\n";
     for (int i = 0; i < Grid.size(); ++i)
        for (int j = 0; j < Grid[i].size(); ++j)
            if(Grid[i][j]!="Lit"&&Grid[i][j]!=" * "&&Grid[i][j]!="UWB"){
                string WB=Grid[i][j];
                MyFile<<"wall_num("+to_string(i+1)+","+to_string(j+1)+","+WB[2]+").\n";
            }
 MyFile<<"\n\n";
     for (int i = 0; i < Grid.size(); ++i)
        for (int j = 0; j < Grid[i].size(); ++j)
            if(Grid[i][j]=="Lit")
                 MyFile<<"light("+to_string(i+1)+","+to_string(j+1)+").\n";
MyFile<<"\n\n";

}

void FileBulding(string fileName){
    ifstream readFile(fileName);
    string readText;
    while (getline (readFile, readText)) {
  // Output the text from the file
  MyFile << readText;
  MyFile << "\n";
}
readFile.close();
}

int main()
{
    SetConsoleTitle("Light-UP By Majd MA");
    int choice;
    vector<vector<string>> Main_Grid={
        {" * ","UWB","Lit"," * "," * "," * "," * "," * "},
        {" * ","Lit","WB2"," * ","Lit","WB3","Lit","UWB"},
        {"Lit","WB3"," * "," * "," * ","Lit","UWB"," * "},
        {" * ","Lit"," * "," * "," * "," * "," * "," * "},
        {" * "," * "," * "," * "," * "," * ","Lit"," * "},
        {" * ","UWB","Lit"," * "," * "," * ","WB3","Lit"},
        {"UWB","Lit","WB4","Lit"," * ","UWB","Lit"," * "},
        {" * "," * ","Lit"," * "," * "," * ","UWB"," * "},
        };
     ifstream readsymbol("Files\\symbol.txt");
     string paint;
     while(getline(readsymbol,paint)){
        cout<< paint<<endl;
     }
     readsymbol.close();
     cout<<"\n\n";
     cout<<"In this application first you will initialize the grid then it will run to start using your grid\n"<<endl;
     cout<<"there is a default built-in grid you can work on or make a new one its up to you"<<endl;
    do{
    cout<<"1.for viewing the current Grid."<<endl;
    cout<<"2.To change the current Settings or editing the Grid"<<endl;
    cout<<"3.To clear cmd"<<endl;
    cout<<"4.To Exit Grid initialize"<<endl;
    cout<<"\n"<<endl;
     cin>>choice;
        switch(choice){
    case 1:
        PrintGride(Main_Grid);
        break;
    case 2:
        {
             system("CLS");
        int element_ADD,raws=3,columns=3;
        do{
        cout<<"You can choose to resize the Grid or to Add lights or Blocks(""Walls"") to it:"<<endl;
        cout<<"1.to Resize the Grid. || Note: The default size of the Grid is (3 x 3).\n\t\t      || Note: when you resize the Grid, it will be back to default...!"<<endl;
        cout<<"2.to Add a light to the Grid"<<endl;
        cout<<"3.to Add a Block to the Grid"<<endl;
        cout<<"4.to get back to the Main Menu"<<endl;
        cout<<"\n";
                cin>>element_ADD;
             switch(element_ADD){
             case 1:
                 {
                 cout<<"Enter the Number of Raws and Columns for the Grid\nNote: The Grid is square (n x n)"<<endl;
                 cout<<"n=\t"; cin>>raws;
                 columns=raws;
                 Main_Grid.resize(0, vector<string>(0));
                 Main_Grid.resize(raws, vector<string>(columns," * "));
                 cout<<"Done!\n"<<endl;

                break;
                 }

             case 2:
                 {
                 int raw=0,column=0;
                 cout<<"You are now in the adding Light Section:"<<endl;
                 cout<<"please enter the number of the raw you want to add to"<<endl; cin>>raw;
                 cout<<"please enter the number of the column you want to add to"<<endl; cin>>column;
                 if(raw>raws||column>columns||raw<=0||column<=0)
                     cout<<"You can`t do that, please check the size of the Grid and try again"<<endl;
                  else{
                    Main_Grid[raw-1][column-1]="Lit";
                    cout<<"Done!\n"<<endl;
                  }
                break;
                 }



             case 3:
                 {
                 int Block_kind;
                 cout<<"There is two kinds of Blocks:"<<endl;
                 cout<<"1.Unweighted  Blocked"<<endl;
                 cout<<"2.Weighted Blocked \n"<<endl;
                 cin>>Block_kind;
            switch(Block_kind){
             case 1:
                 {
                 int raw=0,column=0;
                 cout<<"You are now in the adding Unweighted  Block Section:"<<endl;
                 cout<<"please enter the number of the raw you want to add to"<<endl; cin>>raw;
                 cout<<"please enter the number of the column you want to add to"<<endl; cin>>column;
                  if(raw>raws||column>columns||raw<=0||column<=0)
                     cout<<"You can`t do that, please check the size of the Grid and try again"<<endl;
                    else{
                          Main_Grid[raw-1][column-1]="UWB";
                          cout<<"Done!\n"<<endl;
                    }
                break;
                 }


             case 2:
                 {
                 int raw=0,column=0,wieght;
                 cout<<"You are now in the adding Weighted Block Section:"<<endl;
                 cout<<"Please enter the number of the raw you want to add to"<<endl; cin>>raw;
                 cout<<"Please enter the number of the column you want to add to"<<endl; cin>>column;
                 cout<<"Please enter the weight for this Block"<<endl;cin>>wieght;
                 if(raw>raws||column>columns||raw<=0||column<=0||wieght<0)
                     cout<<"You can`t do that, please check the size of the Grid and try again"<<endl;
                  else{
                    string setValue="WB"+to_string(wieght);
                    Main_Grid[raw-1][column-1]=setValue;
                    cout<<"Done!\n"<<endl;
                  }
                break;
                 }


             default:
                 cout<<"please read the instructions then re-Enter your value......"<<endl;
                 }

                break;
                 }


             case 4:
                 cout<<"\n"<<endl;
                break;

             default:
                  cout<<"please read the instructions then re-Enter your value......"<<endl;
              }
        }while(element_ADD!=4);

        break;
        }

             case 3:
                 {
                     system("CLS");
                     ifstream readsymbol("Files\\symbol.txt");
                     string paint;
                     while(getline(readsymbol,paint)){
                         cout<< paint<<endl;
                        }
                     readsymbol.close();
                     cout<<"\n\n";
                     cout<<"In this application first, you will initialize the grid then it will run to start using your grid\n"<<endl;
                     cout<<"there is a default built-in grid you can work on or make a new one it�s up to you"<<endl;
                 }
                break;
    case 4:
         cout<<"now swi-prolog will start in your cmd \"Magic Right...?!\"."<<endl;
         cout<<"in case there is something not clear, you should open the Readme file for more info.\n\n"<<endl;

        break;
    default:
        cout<<"please read the instructions then re-Enter your value......"<<endl;
        }
    }while(choice!=4);
       FileBulding("Files\\First_Section.pl");
       initializeGrid(Main_Grid);
       FileBulding("Files\\Sec_Section.pl");
       // Close the file
       MyFile.close();
     //int result = system("C:\\Progra~1\\swipl\\bin\\swipl-win.exe");
     system("swipl -s Light-UP.pl");
     cout<<"\nContact with me:\n-majd_ma@outlook.com\n-majd.ma.public@gmail.com\n\nLinked-in:Majd MA.\n\n"<<endl;
     cout<<"Thank you for your support, I really appreciate that."<<endl;


     return 0;
}
